class Dictionary

  attr_accessor :entries, :keywords

  def initialize
    @entries = {}
  end

  def keywords
    keywords_array = entries.keys
    keywords_array.sort
  end

  def add(entry)
    if entry.class == Hash
      entries[entry.keys.first] = entry.values.first
      keywords.push(entry.keys.first)
    else
      entries[entry] = nil
      keywords.push(entry)
    end
  end

  def include?(word)
    self.keywords.include?(word)
  end

  def find(search_word)
    result = {}
    keywords.each do |keyword|
      if keyword[0...search_word.length] == search_word
        result[keyword] = entries[keyword]
      end
    end
    result
  end

  def printable
    result = ''
    entries.each do |key, value|
      result = '[' + key + '] ' + '"' + value + '"' + "\n" + result
    end
    result.rstrip
  end

end
