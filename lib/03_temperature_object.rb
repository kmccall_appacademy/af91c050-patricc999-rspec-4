class Temperature

  attr_reader :fahrenheit, :celsius

  def initialize(options = {})
    if options[:f]
      @fahrenheit = options[:f]
    else
      @celsius = options[:c]
    end
  end

  def in_celsius
    if @celsius.nil?
      @celsius = (@fahrenheit - 32) * 5 / 9
    else
      @celsius
    end
  end

  def in_fahrenheit
    if @fahrenheit.nil?
      @fahrenheit = @celsius.to_f * 9 / 5 + 32
    else
      @fahrenheit
    end
  end

  def self.from_celsius(temp)
    Temperature.new(c: temp)
  end

  def self.from_fahrenheit(temp)
    Temperature.new(f: temp)
  end

end

class Celsius < Temperature

  def initialize(temp)
    @celsius = temp
  end
end

class Fahrenheit < Temperature

  def initialize(temp)
    @fahrenheit = temp
  end
end
