class Book

  def title
    @title
  end

  def title=(title)
    @title = capitalize(title)
  end

  def capitalize(title)
    capitalized_title = ''
    title.split(' ').each_with_index do |word, idx|
      if isforbidden?(word) && idx != 0
        capitalized_title << word + ' '
      else
        capitalized_word = word.chars.first.upcase + word[1..-1]
        capitalized_title << capitalized_word + ' '
      end
    end
    capitalized_title.rstrip
  end

  def isforbidden?(word)
    forbidden_words = ['the', 'a', 'an', 'in', 'of', 'and']
    forbidden_words.include?(word)
  end

end
