class Timer

  attr_accessor :seconds
  attr_reader :time_string

  def initialize
    @seconds = 0
  end

  def time_string
    seconds_place = seconds % 60
    minutes_place = ((seconds - seconds_place) / 60) % 60
    hours_place = (seconds - seconds_place) / 3600
    padded(hours_place) + ':' + padded(minutes_place) + ':' + padded(seconds_place)
  end

  def padded(num)
    if num < 10
      '0' + num.to_s
    else
      num.to_s
    end
  end

end
